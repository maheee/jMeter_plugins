package org.apache.jmeter.visualizers;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.testelement.TestElement;
import org.apache.jmeter.util.JMeterUtils;
import org.apache.jmeter.visualizers.gui.AbstractVisualizer;
import org.productivity.java.syslog4j.SyslogConfigIF;
import org.productivity.java.syslog4j.SyslogIF;
import org.productivity.java.syslog4j.impl.AbstractSyslogConfigIF;
import org.productivity.java.syslog4j.impl.message.processor.structured.StructuredSyslogMessageProcessor;
import org.productivity.java.syslog4j.impl.message.structured.StructuredSyslogMessage;
import org.productivity.java.syslog4j.impl.message.structured.StructuredSyslogMessageIF;
import org.productivity.java.syslog4j.impl.net.tcp.TCPNetSyslogConfig;
import org.productivity.java.syslog4j.impl.net.udp.UDPNetSyslogConfig;

/**
 * Sends data via syslog
 */
public class SyslogReport extends AbstractVisualizer {

  private static final long serialVersionUID = 240L;

  private class Constants {

    public static final String SYSLOG_SERVER_PROTOCOL = "syslogServerProtocol"; //$NON-NLS-1$

    public static final String SYSLOG_SERVER_HOST = "syslogServerHost"; //$NON-NLS-1$

    public static final String SYSLOG_SERVER_PORT = "syslogServerPort"; //$NON-NLS-1$

    public static final String SYSLOG_SERVER_APPLICATION_NAME = "syslogServerApplicationName"; //$NON-NLS-1$

    private static final String DEFAULT_PROTOCOL = "udp"; //$NON-NLS-1$

    private static final String DEFAULT_HOST = "localhost"; //$NON-NLS-1$

    private static final int DEFAULT_PORT = 514;

    private static final String DEFAULT_APPLICATION_NAME = "jMeter";

  }

  private JLabel syslogServerProtocolLabel = new JLabel("Protocol");

  private JLabel syslogServerHostLabel = new JLabel("Server");

  private JLabel syslogServerPortLabel = new JLabel("Port");

  private JLabel syslogServerApplicationNameLabel = new JLabel("Application Name");

  private JTextField syslogServerProtocol = new JTextField();

  private JTextField syslogServerHost = new JTextField();

  private JTextField syslogServerApplicationName = new JTextField();

  private JSpinner syslogServerPort = new JSpinner();

  private final transient Object lock = new Object();

  private SyslogIF syslog = null;

  public SyslogReport() {
    super();
    clearData();
    init();
  }

  @Override
  public String getLabelResource() {
    return "syslog_report_name";
  }

  @Override
  public String getStaticLabel() {
    return "A1 - Syslog Report";
  }

  @Override
  public void add(final SampleResult res) {
    JMeterUtils.runSafe(new Runnable() {

      @Override
      public void run() {
        synchronized (lock) {
          if (res.getErrorCount() <= 0) {
            getSyslog().info(buildSyslogMessage(res));
          } else {
            getSyslog().error(buildSyslogMessage(res));
          }

        }
      }
    });
  }

  private StructuredSyslogMessageIF buildSyslogMessage(SampleResult res) {
    return new StructuredSyslogMessage("JM_REQ", buildSd(res), "Request done!");
  }

  private Map<String, Map<String, String>> buildSd(SampleResult res) {
    Map<String, Map<String, String>> data = new LinkedHashMap<String, Map<String, String>>();
    data.put("request", buildSdElement(res));
    return data;
  }

  private Map<String, String> buildSdElement(SampleResult res) {
    Map<String, String> element = new LinkedHashMap<String, String>();
    element.put("label", res.getSampleLabel(false).replace('"', '\''));
    element.put("thread", res.getThreadName());
    element.put("success", res.getErrorCount() <= 0 ? "yes" : "no");
    element.put("responseCode", res.getResponseCode());
    element.put("responseTime", "" + res.getTime());
    return element;
  }

  public static SyslogIF createSyslog(String syslogProtocol, SyslogConfigIF config) {
    SyslogIF syslog;
    Class<?> syslogClass = config.getSyslogClass();
    try {
      syslog = (SyslogIF) syslogClass.newInstance();
    } catch (Exception e) {
      throw new RuntimeException("Couldn't create Syslog-Instance!", e);
    }
    syslog.initialize(syslogProtocol.toLowerCase(), config);
    return syslog;
  }

  public static SyslogIF createSyslog(String syslogServerProtocol) {
    if ("UDP".equalsIgnoreCase(syslogServerProtocol)) {
      return createSyslog(syslogServerProtocol, new UDPNetSyslogConfig());
    } else if ("TCP".equalsIgnoreCase(syslogServerProtocol)) {
      return createSyslog(syslogServerProtocol, new TCPNetSyslogConfig());
    }
    return null;
  }

  public SyslogIF getSyslog() {
    synchronized (lock) {
      if (syslog == null) {
        syslog = createSyslog(syslogServerProtocol.getText());
        SyslogConfigIF syslogConfig = syslog.getConfig();

        syslogConfig.setHost(syslogServerHost.getText());
        syslogConfig.setPort((Integer) syslogServerPort.getValue());
        syslogConfig.setFacility(7);

        syslogConfig.setIncludeIdentInMessageModifier(false);
        syslogConfig.setSendLocalName(false);
        syslogConfig.setSendLocalTimestamp(false);
        syslogConfig.setUseStructuredData(true);

        syslogConfig.setThrowExceptionOnInitialize(true);
        syslogConfig.setThrowExceptionOnWrite(true);

        if (syslogConfig instanceof AbstractSyslogConfigIF) {
          AbstractSyslogConfigIF syslogConfigIf = (AbstractSyslogConfigIF) syslogConfig;
          syslogConfigIf.setMaxShutdownWait(1000);
        }

        syslog.setStructuredMessageProcessor(new StructuredSyslogMessageProcessor(syslogServerApplicationName.getText()));
      }
      return syslog;
    }
  }

  public void shutdownSyslog() {
    synchronized (lock) {
      if (syslog != null) {
        try {
          syslog.flush();
          syslog.shutdown();
        } catch (Exception e) {

        }
        syslog = null;
      }
    }
  }

  @Override
  public void clearGui() {
    super.clearGui();
    shutdownSyslog();
  }

  /**
   * Clears this visualizer and its model, and forces a repaint of the table.
   */
  @Override
  public void clearData() {
    shutdownSyslog();
  }

  /**
   * Main visualizer setup.
   */
  private void init() {
    this.setLayout(new BorderLayout());

    JPanel configPanel = new JPanel();
    configPanel.setLayout(new BorderLayout());

    Border margin = new EmptyBorder(10, 10, 5, 10);
    configPanel.setBorder(margin);
    configPanel.add(makeTitlePanel(), BorderLayout.NORTH);

    this.add(configPanel, BorderLayout.CENTER);

    JPanel opts = new JPanel();
    opts.setLayout(new GridLayout(0, 2));
    opts.add(syslogServerProtocolLabel);
    opts.add(syslogServerProtocol);
    opts.add(syslogServerHostLabel);
    opts.add(syslogServerHost);
    opts.add(syslogServerPortLabel);
    opts.add(syslogServerPort);
    opts.add(syslogServerApplicationNameLabel);
    opts.add(syslogServerApplicationName);

    configPanel.add(opts, BorderLayout.SOUTH);
    configPanel.add(new JLabel("... nothing to do here ..."), BorderLayout.CENTER);

    syslogServerPort.setValue(Constants.DEFAULT_PORT);
    syslogServerHost.setText(Constants.DEFAULT_HOST);
    syslogServerProtocol.setText(Constants.DEFAULT_PROTOCOL);
    syslogServerApplicationName.setText(Constants.DEFAULT_APPLICATION_NAME);
  }

  @Override
  public void modifyTestElement(TestElement el) {
    super.modifyTestElement(el);
    el.setProperty(Constants.SYSLOG_SERVER_PROTOCOL, syslogServerProtocol.getText(), Constants.DEFAULT_PROTOCOL);
    el.setProperty(Constants.SYSLOG_SERVER_HOST, syslogServerHost.getText(), Constants.DEFAULT_HOST);
    el.setProperty(Constants.SYSLOG_SERVER_PORT, (Integer) syslogServerPort.getValue(), Constants.DEFAULT_PORT);
    el.setProperty(Constants.SYSLOG_SERVER_APPLICATION_NAME, syslogServerApplicationName.getText(), Constants.DEFAULT_APPLICATION_NAME);
    shutdownSyslog();
  }

  @Override
  public void configure(TestElement el) {
    super.configure(el);
    syslogServerProtocol.setText(el.getPropertyAsString(Constants.SYSLOG_SERVER_PROTOCOL, Constants.DEFAULT_PROTOCOL));
    syslogServerHost.setText(el.getPropertyAsString(Constants.SYSLOG_SERVER_HOST, Constants.DEFAULT_HOST));
    syslogServerPort.setValue(el.getPropertyAsInt(Constants.SYSLOG_SERVER_PORT, Constants.DEFAULT_PORT));
    syslogServerApplicationName.setText(el.getPropertyAsString(Constants.SYSLOG_SERVER_APPLICATION_NAME, Constants.DEFAULT_APPLICATION_NAME));
    shutdownSyslog();
  }

}
