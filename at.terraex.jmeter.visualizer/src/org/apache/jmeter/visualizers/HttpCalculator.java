package org.apache.jmeter.visualizers;

import org.apache.jmeter.samplers.SampleResult;

/**
 * Class to calculate various items that don't require all previous results to
 * be saved:
 * - mean = average
 * - standard deviation
 * - minimum
 * - maximum
 */
public class HttpCalculator {

  private double sum = 0;

  private double sumOfSquares = 0;

  private double mean = 0;

  private double deviation = 0;

  private int count = 0;

  private int statCount = 0;

  private long bytes = 0;

  private long maximum = Long.MIN_VALUE;

  private long minimum = Long.MAX_VALUE;

  private int errors = 0;

  private long http100 = 0, http200 = 0, http300 = 0, http400 = 0, http500 = 0;

  private long http401 = 0, http403 = 0, http501 = 0, http503 = 0;

  private long nonHttpError = 0;

  private long assertError = 0;

  private final String label;

  public HttpCalculator() {
    this("");
  }

  public HttpCalculator(String label) {
    this.label = label;
  }

  public void clear() {
    maximum = Long.MIN_VALUE;
    minimum = Long.MAX_VALUE;
    sum = 0;
    sumOfSquares = 0;
    mean = 0;
    deviation = 0;
    count = 0;
    statCount = 0;
    http100 = http200 = http300 = http400 = http500 = 0;
    http401 = http403 = http501 = http503 = 0;
    nonHttpError = 0;
  }

  /**
   * Add the value for a single sample.
   * 
   * @param newValue
   * 
   * @see #addValue(long, int)
   * @deprecated Use {@link #addSample(SampleResult)} instead
   */
  @Deprecated
  public void addValue(long newValue) {
    addValue(newValue, 1);
  }

  /**
   * Add the value for (possibly multiple) samples.
   * Updates the count, sum, min, max, sumOfSqaures, mean and deviation.
   * 
   * @param newValue
   * the total value for all the samples.
   * @param sampleCount
   * number of samples included in the value
   * @param hasErrors
   */
  private void addValue(long newValue, int sampleCount, boolean hasErrors) {
    count += sampleCount;
    if (!hasErrors) {
      double currentVal = newValue;
      sum += currentVal;
      ++statCount;
      if (sampleCount > 1) {
        minimum = Math.min(newValue / sampleCount, minimum);
        maximum = Math.max(newValue / sampleCount, maximum);
        // For n values in an aggregate sample the average value = (val/n)
        // So need to add n * (val/n) * (val/n) = val * val / n
        sumOfSquares += (currentVal * currentVal) / (sampleCount);
      } else { // no point dividing by 1
        minimum = Math.min(newValue, minimum);
        maximum = Math.max(newValue, maximum);
        sumOfSquares += currentVal * currentVal;
      }
      // Calculate each time, as likely to be called for each add
      mean = sum / statCount;
      deviation = Math.sqrt((sumOfSquares / statCount) - (mean * mean));
    }
  }

  private void addValue(long newValue, int sampleCount) {
    addValue(newValue, sampleCount, false);
  }

  public void addBytes(long newValue) {
    bytes += newValue;
  }

  private long startTime = 0;

  private long elapsedTime = 0;

  /**
   * Add details for a sample result, which may consist of multiple samples.
   * Updates the number of bytes read, error count, startTime and elapsedTime
   * 
   * @param res
   * the sample result; might represent multiple values
   * @see #addValue(long, int)
   */
  public void addSample(SampleResult res) {
    addBytes(res.getBytes());
    addValue(res.getTime(), res.getSampleCount(), !SampleResultHelper.isHttpResponse(res));
    errors += res.getErrorCount(); // account for multiple samples
    if (startTime == 0) { // not yet intialised
      startTime = res.getStartTime();
    } else {
      startTime = Math.min(startTime, res.getStartTime());
    }
    elapsedTime = Math.max(elapsedTime, res.getEndTime() - startTime);

    int sampleError = res.getErrorCount();

    if (SampleResultHelper.isHttp200(res)) {
      ++http200;
      assertError += sampleError;
    } else if (SampleResultHelper.isHttp300(res)) {
      ++http300;
      assertError += sampleError;
    } else if (SampleResultHelper.isHttp400(res)) {
      ++http400;
      if (SampleResultHelper.isHttp401(res)) {
        ++http401;
      } else if (SampleResultHelper.isHttp403(res)) {
        ++http403;
      }
    } else if (SampleResultHelper.isHttp500(res)) {
      ++http500;
      if (SampleResultHelper.isHttp501(res)) {
        ++http501;
      } else if (SampleResultHelper.isHttp503(res)) {
        ++http503;
      }
    } else if (SampleResultHelper.isHttp100(res)) {
      ++http100;
      assertError += sampleError;
    } else {
      ++nonHttpError;
    }
  }

  public long getTotalBytes() {
    return bytes;
  }

  public double getMean() {
    return mean;
  }

  public Number getMeanAsNumber() {
    return Long.valueOf((long) mean);
  }

  public double getStandardDeviation() {
    return deviation;
  }

  public long getMin() {
    return minimum;
  }

  public long getMax() {
    return maximum;
  }

  public int getCount() {
    return count;
  }

  public String getLabel() {
    return label;
  }

  /**
   * Returns the raw double value of the percentage of samples with errors
   * that were recorded. (Between 0.0 and 1.0)
   * 
   * @return the raw double value of the percentage of samples with errors
   * that were recorded.
   */
  public double getErrorPercentage() {
    double rval = 0.0;

    if (count == 0) {
      return (rval);
    }
    rval = (double) errors / (double) count;
    return (rval);
  }

  /**
   * Returns the throughput associated to this sampler in requests per second.
   * May be slightly skewed because it takes the timestamps of the first and
   * last samples as the total time passed, and the test may actually have
   * started before that start time and ended after that end time.
   */
  public double getRate() {
    if (elapsedTime == 0) {
      return 0.0;
    }

    return ((double) count / (double) elapsedTime) * 1000;
  }

  /**
   * calculates the average page size, which means divide the bytes by number
   * of samples.
   * 
   * @return average page size in bytes
   */
  public double getAvgPageBytes() {
    if (count > 0 && bytes > 0) {
      return (double) bytes / count;
    }
    return 0.0;
  }

  /**
   * Throughput in bytes / second
   * 
   * @return throughput in bytes/second
   */
  public double getBytesPerSecond() {
    if (elapsedTime > 0) {
      return bytes / ((double) elapsedTime / 1000); // 1000 = millisecs/sec
    }
    return 0.0;
  }

  /**
   * Throughput in kilobytes / second
   * 
   * @return Throughput in kilobytes / second
   */
  public double getKBPerSecond() {
    return getBytesPerSecond() / 1024; // 1024=bytes per kb
  }

  public long getHttp100() {
    return http100;
  }

  public long getHttp200() {
    return http200;
  }

  public long getHttp300() {
    return http300;
  }

  public long getHttp400() {
    return http400;
  }

  public long getHttp500() {
    return http500;
  }

  public long getHttp401() {
    return http401;
  }

  public long getHttp403() {
    return http403;
  }

  public long getHttp501() {
    return http501;
  }

  public long getHttp503() {
    return http503;
  }

  public long getNonHttp() {
    return nonHttpError;
  }

  public long getAssertError() {
    return assertError;
  }
}
