package org.apache.jmeter.visualizers;

import org.apache.jmeter.samplers.SampleResult;

public class SampleResultHelper {

  public static boolean isHttp100(SampleResult res) {
    return res.getResponseCode().startsWith("1");
  }

  public static boolean isHttp200(SampleResult res) {
    return res.getResponseCode().startsWith("2");
  }

  public static boolean isHttp300(SampleResult res) {
    return res.getResponseCode().startsWith("3");
  }

  public static boolean isHttp400(SampleResult res) {
    return res.getResponseCode().startsWith("4");
  }

  public static boolean isHttp500(SampleResult res) {
    return res.getResponseCode().startsWith("5");
  }

  public static boolean isHttp401(SampleResult res) {
    return res.getResponseCode().startsWith("401");
  }

  public static boolean isHttp403(SampleResult res) {
    return res.getResponseCode().startsWith("403");
  }

  public static boolean isHttp501(SampleResult res) {
    return res.getResponseCode().startsWith("501");
  }

  public static boolean isHttp503(SampleResult res) {
    return res.getResponseCode().startsWith("503");
  }

  public static boolean isHttpResponse(SampleResult res) {
    return isHttp200(res) || isHttp300(res) || isHttp400(res) || isHttp500(res) || isHttp100(res);
  }
}
