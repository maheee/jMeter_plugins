package org.apache.jmeter.visualizers;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.apache.jmeter.gui.util.FileDialoger;
import org.apache.jmeter.gui.util.HeaderAsPropertyRenderer;
import org.apache.jmeter.samplers.Clearable;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.save.CSVSaveService;
import org.apache.jmeter.testelement.TestElement;
import org.apache.jmeter.util.JMeterUtils;
import org.apache.jmeter.visualizers.gui.AbstractVisualizer;
import org.apache.jorphan.gui.NumberRenderer;
import org.apache.jorphan.gui.ObjectTableModel;
import org.apache.jorphan.gui.RateRenderer;
import org.apache.jorphan.gui.RendererUtils;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.jorphan.reflect.Functor;
import org.apache.jorphan.util.JOrphanUtils;
import org.apache.log.Logger;

public class HttpSummaryReport extends AbstractVisualizer implements Clearable, ActionListener {

  private static final long serialVersionUID = 240L;

  private static final Logger log = LoggingManager.getLoggerForClass();

  private static final String USE_GROUP_NAME = "useGroupName";

  private static final String SAVE_HEADERS = "saveHeaders";

  private static final String IGNORE_STATISTICAL_SAMPLES = "ignoreStatisticalSamples";

  private static final String TIMESTAMP_FORMAT = "timestampFormat";

  private static final String[] COLUMNS = {
      "Label",
      "Cnt",
      "Avg",
      "Min",
      "Max",
      "StdDev",
      "Error%",
      "Rate",
      "Bandwidth",
      "AvgBytes",
      "HTTP1XX",
      "HTTP2XX",
      "HTTP3XX",
      "HTTP4XX",
      "HTTP5XX",
      "HTTP401",
      "HTTP403",
      "HTTP501",
      "HTTP503",
      "NetError",
      "AssertError",
  };

  private final String TOTAL_ROW_LABEL = JMeterUtils.getResString("aggregate_report_total_label");

  private JTable myJTable;

  private TableColumn[] myJTableColumns; // needed for hiding and showing
                                         // columns

  private JCheckBox[] myJTableColumnsHiders;

  private JScrollPane myScrollPane;

  private final JButton saveTable =
      new JButton(JMeterUtils.getResString("aggregate_graph_save_table"));

  private final JCheckBox saveHeaders = // should header be saved with the data?
  new JCheckBox(JMeterUtils.getResString("aggregate_graph_save_table_header"), true);

  private final JCheckBox useGroupName =
      new JCheckBox(JMeterUtils.getResString("aggregate_graph_use_group_name"));

  private final JCheckBox ignoreStatisticalSamples =
      new JCheckBox("Ignore Statistical Samples");

  private JLabel timestampFormatLabel =
      new JLabel("Timestamp Format");

  private JTextField timestampFormat =
      new JTextField("dd-HH ");

  private transient ObjectTableModel model;

  /**
   * Lock used to protect tableRows update + model update
   */
  private final transient Object lock = new Object();

  private final Map<String, HttpCalculator> tableRows =
      new ConcurrentHashMap<String, HttpCalculator>();

  // Column renderers
  private static final TableCellRenderer[] RENDERERS =
      new TableCellRenderer[] {
          null, // Label
          null, // count
          null, // Mean
          null, // Min
          null, // Max
          new NumberRenderer("#0.00"), // Std Dev.
          new NumberRenderer("#0.00%"), // Error %age
          new RateRenderer("#.0"), // Throughput
          new NumberRenderer("#0.00"), // kB/sec
          new NumberRenderer("#.0"), // avg. pageSize
          null, // http_100
          null, // http_200
          null, // http_300
          null, // http_400
          null, // http_500
          null, // http_401
          null, // http_403
          null, // http_501
          null, // http_503
          null, // non_http
          null, // assert_error
      };

  public HttpSummaryReport() {
    super();
    model = new ObjectTableModel(COLUMNS,
        HttpCalculator.class,// All rows have this class
        new Functor[] {
            new Functor("getLabel"),
            new Functor("getCount"),
            new Functor("getMeanAsNumber"),
            new Functor("getMin"),
            new Functor("getMax"),
            new Functor("getStandardDeviation"),
            new Functor("getErrorPercentage"),
            new Functor("getRate"),
            new Functor("getKBPerSecond"),
            new Functor("getAvgPageBytes"),
            new Functor("getHttp100"),
            new Functor("getHttp200"),
            new Functor("getHttp300"),
            new Functor("getHttp400"),
            new Functor("getHttp500"),
            new Functor("getHttp401"),
            new Functor("getHttp403"),
            new Functor("getHttp501"),
            new Functor("getHttp503"),
            new Functor("getNonHttp"),
            new Functor("getAssertError"),
        },
        new Functor[] { null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null },
        new Class[] { String.class, Long.class, Long.class, Long.class, Long.class,
            String.class, String.class, String.class, String.class, String.class,
            Long.class, Long.class, Long.class, Long.class, Long.class, Long.class, Long.class, Long.class, Long.class, Long.class, Long.class });
    clearData();
    init();
  }

  @Override
  public String getLabelResource() {
    return "http_summary_report";
  }

  @Override
  public String getStaticLabel() {
    return "A1 - Http Summary Report";
  }

  @Override
  public void add(final SampleResult res) {
    if (ignoreStatisticalSamples.isSelected() && (res.getSampleCount() > 1 || res.getResponseCode() == null || res.getResponseCode().isEmpty())) {
      return;
    }
    final String sampleLabelPrefix = timestampFormat.getText().isEmpty() ? "" : new SimpleDateFormat(timestampFormat.getText()).format(Calendar.getInstance().getTime());

    final String sampleLabel = sampleLabelPrefix + res.getSampleLabel(useGroupName.isSelected());
    JMeterUtils.runSafe(new Runnable() {

      @Override
      public void run() {
        HttpCalculator row = null;
        synchronized (lock) {
          row = tableRows.get(sampleLabel);
          if (row == null) {
            row = new HttpCalculator(sampleLabel);
            tableRows.put(row.getLabel(), row);
            model.insertRow(row, model.getRowCount() - 1);
          }
        }
        /*
         * Synch is needed because multiple threads can update the counts.
         */
        synchronized (row) {
          row.addSample(res);
        }
        HttpCalculator tot = tableRows.get(TOTAL_ROW_LABEL);
        synchronized (tot) {
          tot.addSample(res);
        }
        model.fireTableDataChanged();
      }
    });
  }

  /**
   * Clears this visualizer and its model, and forces a repaint of the table.
   */
  @Override
  public void clearData() {
    // Synch is needed because a clear can occur while add occurs
    synchronized (lock) {
      model.clearData();
      tableRows.clear();
      tableRows.put(TOTAL_ROW_LABEL, new HttpCalculator(TOTAL_ROW_LABEL));
      model.addRow(tableRows.get(TOTAL_ROW_LABEL));
    }
  }

  /**
   * Main visualizer setup.
   */
  private void init() {
    this.setLayout(new BorderLayout());

    // MAIN PANEL
    JPanel configPanel = new JPanel();
    JPanel resultPanel = new JPanel();

    configPanel.setLayout(new BorderLayout());
    resultPanel.setLayout(new BorderLayout());

    JTabbedPane tabbedPane = new JTabbedPane();
    tabbedPane.addTab("Configuration", configPanel);
    tabbedPane.addTab("Results", resultPanel);

    Border margin = new EmptyBorder(10, 10, 5, 10);

    tabbedPane.setBorder(margin);

    configPanel.add(makeTitlePanel(), BorderLayout.NORTH);

    myJTable = new JTable(model);
    myJTable.getTableHeader().setDefaultRenderer(new HeaderAsPropertyRenderer() {

      @Override
      protected String getText(Object value, int row, int column) {
        return value.toString();
      }
    });
    myJTable.setPreferredScrollableViewportSize(new Dimension(500, 70));
    RendererUtils.applyRenderers(myJTable, RENDERERS);
    myScrollPane = new JScrollPane(myJTable);

    this.add(tabbedPane, BorderLayout.CENTER);
    saveTable.addActionListener(this);

    JPanel opts1 = new JPanel();
    opts1.setLayout(new BoxLayout(opts1, BoxLayout.LINE_AXIS));
    opts1.add(saveTable);
    opts1.add(saveHeaders);
    opts1.add(new JSeparator(SwingConstants.VERTICAL));
    opts1.add(useGroupName);
    opts1.add(new JSeparator(SwingConstants.VERTICAL));
    opts1.add(timestampFormatLabel);
    opts1.add(timestampFormat);

    JPanel opts2 = new JPanel();
    opts2.setLayout(new BoxLayout(opts2, BoxLayout.PAGE_AXIS));
    opts2.add(ignoreStatisticalSamples);

    JPanel opts = new JPanel();
    opts.setLayout(new BoxLayout(opts, BoxLayout.PAGE_AXIS));
    opts.add(opts1);
    opts.add(opts2);

    resultPanel.add(myScrollPane, BorderLayout.CENTER);
    configPanel.add(opts, BorderLayout.SOUTH);
    configPanel.add(new JLabel("Switch to 'Results' tab to see the results!"), BorderLayout.CENTER);

    // save all columns to be able to hide/show them later
    // and create checkboxes for hiding table columns
    TableColumnModel myJTableColumnModel = myJTable.getColumnModel();
    myJTableColumns = new TableColumn[myJTableColumnModel.getColumnCount()];
    myJTableColumnsHiders = new JCheckBox[myJTableColumnModel.getColumnCount()];

    int columnHidersInCurrentRow = 0;
    JPanel columnHiderRowPanel = null;
    for (int i = 0; i < myJTableColumns.length; ++i) {
      if (columnHidersInCurrentRow >= 7 || columnHiderRowPanel == null) {
        if (columnHiderRowPanel != null) {
          opts.add(columnHiderRowPanel);
        }
        columnHidersInCurrentRow = 0;
        columnHiderRowPanel = new JPanel();
        columnHiderRowPanel.setLayout(new BoxLayout(columnHiderRowPanel, BoxLayout.LINE_AXIS));
      }
      ++columnHidersInCurrentRow;
      myJTableColumns[i] = myJTableColumnModel.getColumn(i);
      myJTableColumnsHiders[i] = new JCheckBox(COLUMNS[i], true);
      myJTableColumnsHiders[i].addChangeListener(new ChangeListener() {

        @Override
        public void stateChanged(ChangeEvent e) {
          updateMyJTableColumns();
        }
      });
      columnHiderRowPanel.add(myJTableColumnsHiders[i]);
    }
    opts.add(columnHiderRowPanel);
  }

  private void updateMyJTableColumns() {
    TableColumnModel tcm = myJTable.getColumnModel();
    for (int i = 0; i < myJTableColumns.length; ++i) {
      tcm.removeColumn(myJTableColumns[i]);
    }

    for (int i = 0; i < myJTableColumns.length; ++i) {
      if (myJTableColumnsHiders[i].isSelected()) {
        tcm.addColumn(myJTableColumns[i]);
      }
    }
  }

  @Override
  public void modifyTestElement(TestElement c) {
    super.modifyTestElement(c);
    c.setProperty(USE_GROUP_NAME, useGroupName.isSelected(), false);
    c.setProperty(SAVE_HEADERS, saveHeaders.isSelected(), true);
    c.setProperty(TIMESTAMP_FORMAT, timestampFormat.getText(), "dd-HH ");
    c.setProperty(IGNORE_STATISTICAL_SAMPLES, ignoreStatisticalSamples.isSelected(), true);
    for (int i = 0; i < COLUMNS.length; ++i) {
      c.setProperty("show_" + COLUMNS[i], myJTableColumnsHiders[i].isSelected(), true);
    }
  }

  @Override
  public void configure(TestElement el) {
    super.configure(el);
    useGroupName.setSelected(el.getPropertyAsBoolean(USE_GROUP_NAME, false));
    saveHeaders.setSelected(el.getPropertyAsBoolean(SAVE_HEADERS, true));
    timestampFormat.setText(el.getPropertyAsString(TIMESTAMP_FORMAT, "dd-HH "));
    ignoreStatisticalSamples.setSelected(el.getPropertyAsBoolean(IGNORE_STATISTICAL_SAMPLES, true));
    for (int i = 0; i < COLUMNS.length; ++i) {
      myJTableColumnsHiders[i].setSelected(el.getPropertyAsBoolean("show_" + COLUMNS[i], true));
    }
  }

  @Override
  public void actionPerformed(ActionEvent ev) {
    if (ev.getSource() == saveTable) {
      JFileChooser chooser = FileDialoger.promptToSaveFile("summary.csv");
      if (chooser == null) {
        return;
      }
      FileWriter writer = null;
      try {
        writer = new FileWriter(chooser.getSelectedFile());
        CSVSaveService.saveCSVStats(model, writer, saveHeaders.isSelected());
      } catch (FileNotFoundException e) {
        log.warn(e.getMessage());
      } catch (IOException e) {
        log.warn(e.getMessage());
      } finally {
        JOrphanUtils.closeQuietly(writer);
      }
    }
  }
}
