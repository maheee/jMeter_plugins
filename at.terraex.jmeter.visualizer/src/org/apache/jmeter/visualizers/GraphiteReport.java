/*
 * This basically works but to really use it, there is some work to do!
 * 
 * Because of that "clearData();" and "init();" are deactivated in the
 * constructor so it doesn't start up at all.
 */

package org.apache.jmeter.visualizers;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.testelement.TestElement;
import org.apache.jmeter.util.JMeterUtils;
import org.apache.jmeter.visualizers.gui.AbstractVisualizer;

/**
 * Sends data to graphite
 */
public class GraphiteReport extends AbstractVisualizer implements ActionListener {

  private static final long serialVersionUID = 240L;

  private static final String USE_GROUP_NAME = "useGroupName"; //$NON-NLS-1$

  private static final String CARBON_SERVER_HOST = "carbonServerHost"; //$NON-NLS-1$

  private static final String CARBON_SERVER_PORT = "carbonServerPort"; //$NON-NLS-1$

  private static final String SEND_INTERVAL = "sendInterval"; //$NON-NLS-1$

  private final JCheckBox useGroupName = new JCheckBox(JMeterUtils.getResString("aggregate_graph_use_group_name")); //$NON-NLS-1$

  private JLabel carbonServerHostLabel = new JLabel(JMeterUtils.getResString("carbon_server_host_label"));

  private JLabel carbonServerPortLabel = new JLabel(JMeterUtils.getResString("carbon_server_port_label"));

  private JTextField carbonServerHost = new JTextField();

  private JSpinner carbonServerPort = new JSpinner();

  private JLabel sendIntervalLabel = new JLabel(JMeterUtils.getResString("send_interval_label"));

  private JSpinner sendInterval = new JSpinner();

  private final JButton carbonServerStart = new JButton(JMeterUtils.getResString("graphite_report_start")); //$NON-NLS-1$

  private final JButton carbonServerStop = new JButton(JMeterUtils.getResString("graphite_report_stop")); //$NON-NLS-1$

  private GraphiteConnector graphiteConnector;

  private Thread graphiteConnectorThread;

  private final transient Object lock = new Object();

  private final Map<String, HttpCalculator> tableRows =
      new ConcurrentHashMap<String, HttpCalculator>();

  private HttpCalculator total = new HttpCalculator();

  public GraphiteReport() {
    super();
    // clearData();
    // init();
  }

  @Override
  public String getLabelResource() {
    return "graphite_report_name"; //$NON-NLS-1$
  }

  @Override
  public String getStaticLabel() {
    return "A1 - Graphite Report";
  }

  @Override
  public void add(final SampleResult res) {
    final String sampleLabel = fixSampleLabelForGraphitePath(res.getSampleLabel(useGroupName.isSelected()));

    JMeterUtils.runSafe(new Runnable() {

      @Override
      public void run() {
        synchronized (lock) {
          // Total
          total.addSample(res);

          // Row
          HttpCalculator row = tableRows.get(sampleLabel);
          if (row == null) {
            row = new HttpCalculator(sampleLabel);
            tableRows.put(sampleLabel, row);
          }
          synchronized (row) {
            row.addSample(res);
          }
        }
      }
    });
  }

  private String fixSampleLabelForGraphitePath(String sampleLabel) {
    return sampleLabel.trim().replace(' ', '_').replace('/', '_');
  }

  /**
   * Clears this visualizer and its model, and forces a repaint of the table.
   */
  @Override
  public void clearData() {
  }

  /**
   * Main visualizer setup.
   */
  private void init() {
    graphiteConnector = new GraphiteConnector();
    graphiteConnectorThread = new Thread(graphiteConnector);
    graphiteConnectorThread.start();

    this.setLayout(new BorderLayout());

    // MAIN PANEL
    JPanel configPanel = new JPanel();

    configPanel.setLayout(new BorderLayout());

    Border margin = new EmptyBorder(10, 10, 5, 10);

    configPanel.setBorder(margin);

    configPanel.add(makeTitlePanel(), BorderLayout.NORTH);

    this.add(configPanel, BorderLayout.CENTER);

    JPanel opts = new JPanel();
    opts.setLayout(new GridLayout(3, 2));
    opts.add(carbonServerHostLabel);
    opts.add(carbonServerHost);
    opts.add(carbonServerPortLabel);
    opts.add(carbonServerPort);
    opts.add(sendIntervalLabel);
    opts.add(sendInterval);
    opts.add(carbonServerStart);
    opts.add(carbonServerStop);

    carbonServerStart.addActionListener(this);
    carbonServerStop.addActionListener(this);
    carbonServerStart.setEnabled(true);
    carbonServerStop.setEnabled(false);

    configPanel.add(opts, BorderLayout.SOUTH);
    configPanel.add(new JLabel("Switch to 'Results' tab to see the results!"), BorderLayout.CENTER);
  }

  @Override
  public void modifyTestElement(TestElement c) {
    super.modifyTestElement(c);
    c.setProperty(USE_GROUP_NAME, useGroupName.isSelected(), false);
    c.setProperty(CARBON_SERVER_HOST, carbonServerHost.getText(), "10.0.0.2");
    c.setProperty(CARBON_SERVER_PORT, (Integer) carbonServerPort.getValue(), 2003);
    c.setProperty(SEND_INTERVAL, (Integer) sendInterval.getValue(), 1000);
  }

  @Override
  public void configure(TestElement el) {
    super.configure(el);
    useGroupName.setSelected(el.getPropertyAsBoolean(USE_GROUP_NAME, false));
    carbonServerHost.setText(el.getPropertyAsString(CARBON_SERVER_HOST, "10.0.0.2"));
    carbonServerPort.setValue(el.getPropertyAsInt(CARBON_SERVER_PORT, 2003));
    sendInterval.setValue(el.getPropertyAsInt(SEND_INTERVAL, 1000));
  }

  class GraphiteConnector implements Runnable {

    private ConcurrentLinkedQueue<String> carbonMessages = new ConcurrentLinkedQueue<String>();

    private boolean stop = true;

    private Socket socket = null;

    public void sendMessages(long time) {
      carbonMessages.add(createMessage("jmeter.response.total.responseTime", total.getMean(), time));
      carbonMessages.add(createMessage("jmeter.response.total.100.cnt", total.getHttp100(), time));
      carbonMessages.add(createMessage("jmeter.response.total.200.cnt", total.getHttp200(), time));
      carbonMessages.add(createMessage("jmeter.response.total.300.cnt", total.getHttp300(), time));
      carbonMessages.add(createMessage("jmeter.response.total.400.cnt", total.getHttp400(), time));
      carbonMessages.add(createMessage("jmeter.response.total.500.cnt", total.getHttp500(), time));
      carbonMessages.add(createMessage("jmeter.response.total.assErr.cnt", total.getAssertError(), time));
      carbonMessages.add(createMessage("jmeter.response.total.sockErr.cnt", total.getNonHttp(), time));

      for (String key : tableRows.keySet()) {
        carbonMessages.add(createMessage("jmeter.response." + key + ".responseTime", tableRows.get(key).getMean(), time));
        carbonMessages.add(createMessage("jmeter.response." + key + ".100.cnt", tableRows.get(key).getHttp100(), time));
        carbonMessages.add(createMessage("jmeter.response." + key + ".200.cnt", tableRows.get(key).getHttp200(), time));
        carbonMessages.add(createMessage("jmeter.response." + key + ".300.cnt", tableRows.get(key).getHttp300(), time));
        carbonMessages.add(createMessage("jmeter.response." + key + ".400.cnt", tableRows.get(key).getHttp400(), time));
        carbonMessages.add(createMessage("jmeter.response." + key + ".500.cnt", tableRows.get(key).getHttp500(), time));
        carbonMessages.add(createMessage("jmeter.response." + key + ".assErr.cnt", tableRows.get(key).getAssertError(), time));
        carbonMessages.add(createMessage("jmeter.response." + key + ".sockErr.cnt", tableRows.get(key).getNonHttp(), time));
      }
    }

    private String createMessage(String name, Object nr, long time) {
      return name + " " + nr + " " + time;
    }

    public void start() {
      clear();
      stop = false;
    }

    public void stop() {
      stop = true;
    }

    private void clear() {
      total.clear();
      for (String key : tableRows.keySet()) {
        HttpCalculator row = tableRows.get(key);
        synchronized (row) {
          row.clear();
        }
      }
    }

    private void connect() {
      String host = carbonServerHost.getText();
      int port = (Integer) carbonServerPort.getValue();

      try {
        socket = new Socket(host, port);
      } catch (Exception e) {
        socket = null;
        e.printStackTrace();
      }
    }

    private void disconnect() {
      try {
        socket.close();
      } catch (IOException e) {
      }
      socket = null;
    }

    private void send(String msg) {
      if (socket == null) {
        connect();
      }
      if (socket != null) {
        try {
          socket.getOutputStream().write(msg.getBytes());
          socket.getOutputStream().write("\n".getBytes());
        } catch (IOException e) {
          disconnect();
        }
      }
    }

    @Override
    public void run() {
      long lastRun = System.currentTimeMillis();
      while (true) {
        while (!stop) {
          if (System.currentTimeMillis() - lastRun > 1000) {
            lastRun = System.currentTimeMillis();
            synchronized (lock) {
              sendMessages(lastRun);
              clear();
            }
          }
          String message;
          while ((message = carbonMessages.poll()) != null) {
            send(message);
          }
          sleep(1);
        }
        disconnect();
        sleep(1000);
      }
    }

    private void sleep(int ms) {
      try {
        Thread.sleep(ms);
      } catch (InterruptedException e) {
      }
    }
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (e.getSource() == carbonServerStart) {
      graphiteConnector.start();
      carbonServerStart.setEnabled(false);
      carbonServerStop.setEnabled(true);
    } else if (e.getSource() == carbonServerStop) {
      graphiteConnector.stop();
      carbonServerStart.setEnabled(true);
      carbonServerStop.setEnabled(false);
    }
  }

}
