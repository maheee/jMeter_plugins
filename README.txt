===============================================================================
  jMeter 2.11 TEX
===============================================================================

The TEX version of jMeter 2.11 adds some additional features to jMeter.

Changes:
- it allows to reset remote clients and reconnect to them after failure

-------------------------------------------------------------------------------

How to build the TEX version of jMeter 2.11:

1. Check out original jMeter 2.11 from:
   http://svn.apache.org/repos/asf/jmeter/tags/v2_11/
2. Apply patch "jMeter_2.11.diff"
3. Run "ant download_jars"
4. Run "ant clean install"
5. Run "ant distribution -Djmeter.version=2.11TEX -Ddisable-svnCheck=true -Ddisable-check-versions=true"
5. have fun!

-------------------------------------------------------------------------------