package org.apache.jmeter.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.jmeter.control.Controller;
import org.apache.jmeter.control.GenericController;
import org.apache.jmeter.samplers.Sampler;
import org.apache.jmeter.testelement.TestElement;
import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;

public class MultiIntervalController extends GenericController implements Serializable {

  private static final long serialVersionUID = 1L;

  private int idleSleepTime = 0;

  private double intervalMultiplier = 0;

  private static final Logger log = LoggingManager.getLoggerForClass();

  private List<MultiIntervalControllerElement> elements = null;

  private Controller activeController = null;

  private void updateProperties() {
    idleSleepTime = getProperty("idleSleepTime").getIntValue();
    intervalMultiplier = getProperty("intervalMultiplier").getDoubleValue();
  }

  /**
   * Called to initialize a controller at the beginning of a test iteration.
   */
  @Override
  public void initialize() {
    updateProperties();

    log.info("Idle Sleep Time: " + idleSleepTime);
    elements = new ArrayList<MultiIntervalControllerElement>();
    for (TestElement element : getSubControllers()) {
      String name = element.getName();
      long interval = getIntervalFromName(name);
      long realInterval = (long) (interval * intervalMultiplier);
      if (interval >= 0) {
        log.info("Created element '" + element.getName() + "' with interval '" + interval / 1000 + "s', real interval (including multiplier) '" + realInterval / 1000 + "s'!");
        elements.add(new MultiIntervalControllerElement(element, realInterval));
      }
    }
  }

  private long getIntervalFromName(String name) {
    try {
      String part = name.split(";")[0];
      String number = part.substring(0, part.length() - 1);
      char unit = part.charAt(part.length() - 1);
      long mod = 1;
      switch (unit) {
        case 's':
        case 'S':
          mod = 1000;
          break;
        case 'm':
        case 'M':
          mod = 1000 * 60;
          break;
        case 'h':
        case 'H':
          mod = 1000 * 60 * 60;
          break;
        default:
          throw new Exception("Couldn't parse interval unit!");
      }

      return Long.parseLong(number) * mod;
    } catch (Exception e) {
      log.error("Couldn't parse interval. Element '" + name + "' has a faulty name!", e);
      return Long.MIN_VALUE;
    }
  }

  /**
   * Delivers the next Sampler or null
   * 
   * @return org.apache.jmeter.samplers.Sampler or null
   */
  @Override
  public Sampler next() {
    // if there is an active controller, we try to use it
    if (activeController != null) {
      try {
        Sampler sampler = activeController.next();
        if (sampler != null) {
          return sampler;
        }
      } catch (Exception e) {
      }
      // controller is done, we need to find a new element
      activeController = null;
    }

    // searching for the first element which is ready to be executed
    MultiIntervalControllerElement nextElement;
    try {
      nextElement = findNextElement();
    } catch (InterruptedException e) {
      // we got interrupted and need to quit asap
      return null;
    }

    if (nextElement.isSampler()) {
      // found a sampler
      return (Sampler) nextElement.call();
    } else {
      // found a controller, setting it as active and starting recursion
      activeController = (Controller) nextElement.call();
      return next();
    }
  }

  private MultiIntervalControllerElement findNextElement() throws InterruptedException {
    while (true) {
      for (MultiIntervalControllerElement element : elements) {
        if (element.isReady()) {
          return element;
        }
      }
      Thread.sleep(idleSleepTime);
    }
  }

  /**
   * Indicates whether the Controller is done delivering Samplers for the rest
   * of the test.
   * 
   * When the top-level controller returns true to JMeterThread,
   * the thread is complete.
   * 
   * @return boolean
   */
  @Override
  public boolean isDone() {
    // we are never done, this controller executes until it gets interrupted
    return false;
  }

}
