package org.apache.jmeter.controller;

import org.apache.jmeter.samplers.Sampler;
import org.apache.jmeter.testelement.TestElement;

public class MultiIntervalControllerElement {

  private final long interval;

  private final TestElement element;

  private long lastCall;

  private boolean isSampler;

  public MultiIntervalControllerElement(TestElement element, long interval) {
    this.element = element;
    this.interval = interval;
    this.lastCall = System.currentTimeMillis() - (long) (interval * Math.random());
    this.isSampler = element instanceof Sampler;
  }

  public boolean isReady() {
    return lastCall + interval < System.currentTimeMillis();
  }

  public boolean isSampler() {
    return isSampler;
  }

  public boolean isController() {
    return !isSampler();
  }

  public TestElement call() {
    lastCall = System.currentTimeMillis();
    return element;
  }

}
