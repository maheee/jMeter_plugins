package org.apache.jmeter.controller;

import java.awt.GridLayout;
import java.text.DecimalFormat;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import org.apache.jmeter.control.gui.AbstractControllerGui;
import org.apache.jmeter.testelement.TestElement;
import org.apache.jmeter.testelement.property.DoubleProperty;
import org.apache.jmeter.testelement.property.IntegerProperty;
import org.apache.jmeter.testelement.property.JMeterProperty;
import org.apache.jorphan.gui.layout.VerticalLayout;

public class MultiIntervalControllerGui extends AbstractControllerGui {

  private static final long serialVersionUID = 1L;

  private JLabel idleSleepTimeLabel = new JLabel("Idle Sleep Time");

  private JSpinner idleSleepTime = new JSpinner();

  private JLabel intervalMultiplierLabel = new JLabel("Interval Multiplier");

  private JSpinner intervalMultiplier = createDecimalSpinner(0.00, 100.00, 0.01, 1.00);

  private JSpinner createDecimalSpinner(double min, double max, double stepSize, double value) {
    JSpinner spinner = new JSpinner(new SpinnerNumberModel(value, min, max, stepSize));
    JSpinner.NumberEditor editor = (JSpinner.NumberEditor) spinner.getEditor();

    DecimalFormat format = editor.getFormat();
    format.setMinimumFractionDigits(3);

    return spinner;
  }

  public MultiIntervalControllerGui() {
    setLayout(new VerticalLayout(5, VerticalLayout.BOTH, VerticalLayout.TOP));
    setBorder(makeBorder());
    add(makeTitlePanel());

    addLabel("");
    addLabel("This controller is able to execute its direct subelements in individual intervals.");
    addLabel("");
    addLabel("To make this work, all direct subelements have to have the desired interval in their name like this:");
    addLabel("72s; This is the name -> this will be executed every 72 seconds");
    addLabel("5m; This is the name -> this will be executed every 5 minutes");
    addLabel("2h; This is the name -> this will be executed every 2 hours");
    addLabel("");
    addLabel("If a name is faulty, the element won't execute!");
    addLabel("Should this happen, you can see it in the log file!");
    addLabel("");
    addLabel("If there is nothing to execute, the controller will sleep a while and then check again.");
    addLabel("");
    addLabel("");

    JPanel opts = new JPanel();
    opts.setLayout(new GridLayout(0, 2));
    opts.add(idleSleepTimeLabel);
    opts.add(idleSleepTime);
    opts.add(intervalMultiplierLabel);
    opts.add(intervalMultiplier);

    add(opts);
  }

  private void addLabel(String text) {
    add(new JLabel(text));
  }

  @Override
  public String getLabelResource() {
    return "multiinterval_controller_title";
  }

  @Override
  public String getStaticLabel() {
    return "A1 - Multi Interval Controller";
  }

  @Override
  public TestElement createTestElement() {
    MultiIntervalController mic = new MultiIntervalController();
    configureTestElement(mic);
    modifyTestElement(mic);
    return mic;
  }

  /**
   * GUI components are responsible for populating TestElements they create with
   * the data currently held in the GUI components. This method should overwrite
   * whatever data is currently in the TestElement as it is called after a user
   * has filled out the form elements in the gui with new information.
   */
  @Override
  public void modifyTestElement(TestElement element) {
    element.setProperty(new IntegerProperty("idleSleepTime", (Integer) idleSleepTime.getValue()));
    element.setProperty(new DoubleProperty("intervalMultiplier", (Double) intervalMultiplier.getValue()));
  }

  /**
   * The GUI must be able to extract the data from the TestElement and update
   * all GUI fields to represent those data. This method is called to allow
   * JMeter to show the user the GUI that represents the test element's data.
   */
  @Override
  public void configure(TestElement element) {
    super.configure(element);

    JMeterProperty idleSleepTimeProp = element.getProperty("idleSleepTime");
    JMeterProperty intervalMultiplierProp = element.getProperty("intervalMultiplier");

    idleSleepTime.setValue(idleSleepTimeProp != null ? idleSleepTimeProp.getIntValue() : 5000);
    intervalMultiplier.setValue(intervalMultiplierProp != null ? intervalMultiplierProp.getDoubleValue() : 1.0d);
  }

}
