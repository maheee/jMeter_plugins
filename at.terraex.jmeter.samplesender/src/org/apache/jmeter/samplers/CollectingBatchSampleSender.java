package org.apache.jmeter.samplers;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.jorphan.logging.LoggingManager;
import org.apache.log.Logger;

public class CollectingBatchSampleSender extends AbstractSampleSender implements Serializable {

  private static final long serialVersionUID = 240L;

  static final Logger log = LoggingManager.getLoggerForClass();

  private RemoteSampleListener listener;

  private ConcurrentHashMap<String, CollectedSampleResult> cachedEvents = new ConcurrentHashMap<String, CollectedSampleResult>();

  private long sendInterval = 60000;

  private long nextSendTime = System.currentTimeMillis() + sendInterval;

  public CollectingBatchSampleSender() {
    log.info("Using CollectingBatchSampleSender for this run.");
  }

  public CollectingBatchSampleSender(RemoteSampleListener listener) {
    this.listener = listener;
    log.info("Using CollectingBatchSampleSender for this run.");
  }

  @Override
  public void sampleOccurred(SampleEvent event) {
    SampleResult result = event.getResult();

    CollectedSampleResult old = cachedEvents.putIfAbsent(result.getSampleLabel(false), new CollectedSampleResult(result));
    if (old != null) {
      old.addSampleResult(result);
    }

    if (timeToSend()) {
      try {
        sendBatch();
      } catch (RemoteException e) {
        log.warn("sampleOccurred", e);
      }
    }
  }

  private boolean timeToSend() {
    long now = System.currentTimeMillis();
    if (nextSendTime < now) {
      nextSendTime = now + sendInterval;
      return true;
    }
    return false;
  }

  private synchronized void sendBatch() throws RemoteException {
    ConcurrentHashMap<String, CollectedSampleResult> oldCachedEvents;

    synchronized (this.cachedEvents) {
      oldCachedEvents = cachedEvents;
      this.cachedEvents = new ConcurrentHashMap<String, CollectedSampleResult>();
    }

    if (oldCachedEvents.size() > 0) {
      log.info("Sending batch with size: " + oldCachedEvents.size());
      List<SampleEvent> events = new ArrayList<SampleEvent>();
      for (SampleResult res : oldCachedEvents.values()) {
        events.add(new SampleEvent(res, res.getThreadName()));
      }
      listener.processBatch(events);
    }
  }

  @Override
  public void testEnded(String host) {
    try {
      sendBatch();
    } catch (RemoteException e) {
      log.warn("testEnded(hostname)", e);
    }
  }

}
