package org.apache.jmeter.samplers;

import java.io.Serializable;

public class VariableSampleSender extends AbstractSampleSender implements Serializable {

  private SampleSender realSampleSender = null;

  private RemoteSampleListener listener;

  public VariableSampleSender(RemoteSampleListener listener) {
    this.listener = listener;
  }

  private void checkRealSampleSender() {
    if (realSampleSender == null) {
      realSampleSender = MySampleSenderFactory.getInstance(listener);
    }
  }

  @Override
  public void testEnded(String host) {
    checkRealSampleSender();
    realSampleSender.testEnded(host);
  }

  @Override
  public void sampleOccurred(SampleEvent e) {
    checkRealSampleSender();
    realSampleSender.sampleOccurred(e);
  }

}
