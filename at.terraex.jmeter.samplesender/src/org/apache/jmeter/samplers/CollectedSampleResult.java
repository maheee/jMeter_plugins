package org.apache.jmeter.samplers;

import java.io.Serializable;

public class CollectedSampleResult extends SampleResult implements Serializable {

  private static final long serialVersionUID = 240L;

  private int errorCount;

  private long elapsed;

  public CollectedSampleResult() {
  }

  public CollectedSampleResult(SampleResult sampleResult) {
    this.setSampleLabel(sampleResult.getSampleLabel());
    this.addSampleResult(sampleResult);
  }

  public synchronized void addSampleResult(SampleResult sampleResult) {
    this.setSampleCount(this.getSampleCount() + sampleResult.getSampleCount());
    this.setBytes(this.getBytes() + sampleResult.getBytes());

    if (!sampleResult.isSuccessful()) {
      errorCount++;
      this.setSuccessful(false);
    }

    if (getStartTime() == 0) {
      this.setStartTime(sampleResult.getStartTime());
    } else {
      this.setStartTime(Math.min(getStartTime(), sampleResult.getStartTime()));
    }
    this.setEndTime(Math.max(getEndTime(), sampleResult.getEndTime()));

    setLatency(getLatency() + sampleResult.getLatency());

    elapsed += sampleResult.getTime();
  }

  @Override
  public int getErrorCount() {
    return errorCount;
  }

  @Override
  public void setErrorCount(int errorCount) {
    this.errorCount = errorCount;
  }

  @Override
  public long getTime() {
    return elapsed;
  }

}
